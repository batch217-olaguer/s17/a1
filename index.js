// console.log("Hello World!");
console.log("aL s17-a1");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userInfo(){
		let userName = prompt("What is your name?")
		let userAge = prompt("How old are you?")
		let userAddress = prompt("Where do you live?")

		console.log("Hello, " +userName+ ".");
		console.log("You are " +userAge+ " years old.");
		console.log("You live in " +userAddress+ ".");
	}
userInfo();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	let top5FavoriteBands = function favoriteBands (){
		console.log("1. All Time Low");
		console.log("2. Ed Sheeran");
		console.log("3. Adie");
		console.log("4. Charlie Puth");
		console.log("5 Arthur Nerry");
	}

	top5FavoriteBands();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	let top5FavoriteMovies = function favoriteMoviesWithRatings (){
		let favMovie1 = ("Treasure Planet");
		let favMovie2 = ("Les Miserables");
		let favMovie3 = ("Avenger's Endgame");
		let favMovie4 = ("Your Name");
		let favMovie5 = ("The Choice")

		let favMovieRating1 = ("69%");
		let favMovieRating2 = ("69%");
		let favMovieRating3 = ("94%");
		let favMovieRating4 = ("98%");
		let favMovieRating5 = ("11%");

		console.log(favMovie1);
		console.log("Rotten Tomatoes Rating: " +favMovieRating1);
		console.log(favMovie2);
		console.log("Rotten Tomatoes Rating: " +favMovieRating2);
		console.log(favMovie3);
		console.log("Rotten Tomatoes Rating: " +favMovieRating3);
		console.log(favMovie4);
		console.log("Rotten Tomatoes Rating: " +favMovieRating4);
		console.log(favMovie5);
		console.log("Rotten Tomatoes Rating: " +favMovieRating5);
	}

	top5FavoriteMovies();



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

/*console.log(friend1);
console.log(friend2);
console.log(friend3);
*/